// @flow
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Auth0Provider, useAuth0 } from 'src/react-auth0-spa';

const Nav = styled.nav`
  background: lightgray;
  padding: 20px;
  display: flex;
  justify-content: flex-end;
`;
const Item = styled.div`
  display: flex;
  align-items: center;
  padding-right: 20px;
  cursor: pointer;
`;
const Main = styled.main`
  padding: 20px;
`;
const Img = styled.img`
  width: 18px;
  border-radius: 50%;
  margin: 0 10px 0 0;
`;

const NavBar = () => {
  const {
    isAuthenticated, loginWithRedirect, logout,
    loading, user,
  } = useAuth0();

  return (
    <Nav>
      { !isAuthenticated && !loading && (
        <Item
          onClick={ () => loginWithRedirect({}) }
        >log in</Item>
      ) }
      { isAuthenticated && !loading && (
        <>
          <Item>
            <Img src={ user.picture } alt='profile' />
            { user.name }
          </Item>
          <Item
            onClick={ () => logout() }
          >log out</Item>
        </>
      ) }
      { loading && 'loading...' }
    </Nav>
  );
};

const auth0config = {
  domain: 'omatviiv.eu.auth0.com',
  client_id: 'E5FJ3QqfkRWzikV4pyc18JoMTrbgHxD0',
  redirect_uri: 'https://matviiv.com/auth0test',
};
const onRedirectCallback = appState => {
  window.history.replaceState(
    {},
    document.title,
    appState && appState.targetUrl
      ? appState.targetUrl
      : window.location.pathname
  );
};

const App = () => (
  <Auth0Provider
    domain={ auth0config.domain }
    client_id={ auth0config.client_id }
    redirect_uri={ auth0config.redirect_uri }
    onRedirectCallback={ onRedirectCallback }
  >
    <NavBar />
    <Main>
      Application
    </Main>
  </Auth0Provider>
);

export default App;
