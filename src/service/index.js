const path = require('path');
const express = require('express');

const app = express();
const port = 80;

app.use((req, res, next) => {
  console.log(`== ${ req.method } ${ req.url }`);
  next();
});
app.use(express.static(path.resolve('./dist')));

app.listen(port, () => console.info(`listening on port ${ port }`));
