# start development
1. webpack dev server
  - `npm run local` - starts webpack dev server
  - http://localhost:9000 - the app

2. node + webpack
  in case you wan't to check locally how its served on dev|prod environment
  - `npm run start:local` - to start node service
  - `npm run build:local` - to build ui with webpack in dev mode with watch mode
  - http://localhost - the app
