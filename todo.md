- include service into the build (dist)
  - move all ui files > dist/ui
  - put servcie files > dist/service
  - update .gitlab-ci.yml accordingly
  - local build will use service from src/servcie and prod build will use
    it from dist/service (test or review environment will use prod build)
- use .env file to store environment variables
  - install dotenv package for node
  - make static files location configurable through STATIC_PATH
    - locally STATIC_PATH=dist/ui
    - define variable in .gitlab-ci.yml STATIC_PATH=ui
- create docker file to serve project with node

# done
+ build .gitlab-ci.yml and try to run pipeline
+ serve project with node
+ update package.json with project specific data
  name (unique on npmjs), description, repository, bugs and homepage urls


# Convention
Unordered list with:
- `*` means in progress items
- `-` means todo items
- `+` means completed items
