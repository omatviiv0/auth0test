const path = require('path');
const pkg = require('./package.json');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const FlowWebpackPlugin = require('flow-webpack-plugin');

module.exports = (env, args) => {
  const mode = args.mode;

  return {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
      alias: {
        src: path.resolve(__dirname, 'src/'),
        assets: path.resolve(__dirname, 'src/assets/'),
      },
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          include: [
            path.resolve(__dirname, 'src'),
            // include other components that have to be processed by webpack:
            // path.resolve(__dirname, 'node_modules/omtv-react-input'),
          ],
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env',
                '@babel/preset-react',
                '@babel/preset-flow',
              ],
            },
          },
        },
        {
          test: /\.css/,
          use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        filename: 'index.html',
        title: pkg.name,
        uri: mode === 'production' ? '/auth0test/' : '/',
        template: path.resolve(__dirname, 'src/index.html'),
      }),
      new CleanWebpackPlugin(['dist']),
      new FaviconsWebpackPlugin({
        logo: './src/assets/images/favicon.png',
        prefix: mode === 'production' ? '/auth0test/assets' : 'assets',
      }),
      new FlowWebpackPlugin(),
    ],
    devServer: {
      contentBase: './dist',
      port: 9000,
    },
  };
};
